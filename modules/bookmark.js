/**
 * (C) Copyright 2011-2012 Peter Lunicks
 *
 * Use, modification, and distribution are subject to the terms specified in the
 * COPYING file.
**/

function get_bookmarks_for_uri(uri) {
    return nav_bookmarks_service.getBookmarkIdsForURI(make_uri(uri), {});
}
function set_bookmark_title(id, title) {
    return nav_bookmarks_service.setItemTitle(id, title);
}
function get_bookmark_title(id) {
    return nav_bookmarks_service.getItemTitle(id);
}
function get_bookmark_uri(id) {
    return nav_bookmarks_service.getBookmarkURI(id);
}
function set_bookmark_uri(id, uri) {
    return nav_bookmarks_service.changeBookmarkURI(id, make_uri(uri));
}
function is_bookmarked(uri) {
    return nav_bookmarks_service.isBookmarked(make_uri(uri))
}

function remove_bookmark(id) {
    return nav_bookmarks_service.removeItem(id);
}

define_keymap("bookmark_edit_keymap", $parent = minibuffer_keymap);
function bookmark_edit_modality (buffer, elem) {
    buffer.keymaps.push(bookmark_edit_keymap);
}

function kill_current_bookmark (I) {
    var id = I.window.minibuffer.current_bookmark_id;
    if (typeof id == 'undefined' || id == null) {
        minibuffer_abort(I.window);
        I.minibuffer.message("Not currently editing a bookmark.");
        return;
    }
    var uri_string = get_bookmark_uri(id).spec;
    var title = get_bookmark_title(id);
    remove_bookmark(id);
    minibuffer_abort(I.window);
    I.minibuffer.message("Removed bookmark: " + uri_string + " - " + title);
}

interactive("kill-current-bookmark",
            "Remove the bookmark that is currently being edited in the minibuffer.",
            kill_current_bookmark);

define_key(bookmark_edit_keymap, "M-k", "kill-current-bookmark");

function bookmark_edit_current_bookmark_uri (I) {
    var id = I.window.minibuffer.current_bookmark_id;
    if (typeof id == 'undefined' || id == null) {
        minibuffer_abort(I.window);
        I.minibuffer.message("Not currently editing a bookmark.");
        return;
    }
    var list = keymap_lookup_command([bookmark_edit_keymap], "kill-current-bookmark");
    var keymsg = "";
    if (list.length)
        keymsg = " (" + list.join(", ") + " to remove)";

    try {
        var uri_string = yield I.minibuffer.read($prompt = "Edit bookmark URI"+keymsg+":",
                                                 $initial_value = get_bookmark_uri(id).spec || "",
                                                 $keymap = bookmark_edit_keymap);
    } catch (e) {
        // e.g. escape (C-g) -- don't modify the URI
        return;
    }
    set_bookmark_uri(id, make_uri(uri_string));
}

interactive("bookmark-edit-current-bookmark-uri",
            "Switch to editing the URI of the currently-edited bookmark.",
            bookmark_edit_current_bookmark_uri);
define_key(bookmark_edit_keymap, "M-e", "bookmark-edit-current-bookmark-uri");

function bookmark_add_or_edit (I) {
    var element = yield read_browser_object(I);
    var spec = load_spec(element);
    var uri_string = load_spec_uri_string(spec);
    var panel;

    if (is_bookmarked(uri_string)) {
        // If the URI is bookmarked, edit the first instance.
        var id = get_bookmarks_for_uri(uri_string)[0];

        var list = keymap_lookup_command([bookmark_edit_keymap], "kill-current-bookmark");
        var keymsg = "";
        if (list.length)
            keymsg = " (" + list.join(", ") + " to remove)";

        var list_uri = keymap_lookup_command([bookmark_edit_keymap], "bookmark-edit-current-bookmark-uri");
        var keymsg_uri = "";
        if (list_uri.length)
            keymsg_uri = " (" + list_uri.join(", ") + " to edit URL)";

        panel = create_info_panel(I.window, "bookmark-panel",
                                  [["bookmarking",
                                    element_get_operation_label(element, "Editing bookmark for"),
                                    uri_string
                                   + keymsg_uri
                          ]]);
        try {
            I.window.minibuffer.current_bookmark_id = id;
            var title = yield I.minibuffer.read($prompt = "Edit bookmark title"+keymsg+":",
                                                $initial_value = get_bookmark_title(id) || "",
                                                $keymap = bookmark_edit_keymap);
            delete I.window.minibuffer.current_bookmark_id;
        } catch (e) {
            // e.g. escape (C-g) -- don't modify the title
            return;
        } finally {
            panel.destroy();
        }
        set_bookmark_title(id, title);
        I.minibuffer.message("Edited bookmark: " + uri_string + " - " + title);
    } else {
        panel = create_info_panel(I.window, "bookmark-panel",
                                  [["bookmarking",
                                    element_get_operation_label(element, "Bookmarking"),
                                    uri_string]]);
        try {
            var title = yield I.minibuffer.read($prompt = "Bookmark with title:", $initial_value = load_spec_title(spec) || "");
        } finally {
            panel.destroy();
        }
        add_bookmark(uri_string, title);
        I.minibuffer.message("Added bookmark: " + uri_string + " - " + title);
    }
};

function bookmark_add (I) {
    var element = yield read_browser_object(I);
    var spec = load_spec(element);
    var uri_string = load_spec_uri_string(spec);
    var panel;
    panel = create_info_panel(I.window, "bookmark-panel",
                              [["bookmarking",
                                element_get_operation_label(element, "Bookmarking"),
                                uri_string]]);
    try {
        var title = yield I.minibuffer.read($prompt = "Bookmark with title:", $initial_value = load_spec_title(spec) || "");
    } finally {
        panel.destroy();
    }
    add_bookmark(uri_string, title);
    I.minibuffer.message("Added bookmark: " + uri_string + " - " + title);
};

interactive("bookmark",
            "Add or edit a bookmark."
            + "C-u adds a new bookmark even if one already exists for this URI.",
            alternates(bookmark_add_or_edit, bookmark_add),
            $browser_object = browser_object_frames);

provide("bookmark");
