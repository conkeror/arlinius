/**
 * (C) Copyright 2008 Nelson Elhage
 * (C) Copyright 2011 Peter Lunicks
 *
 * Use, modification, and distribution are subject to the terms specified in the
 * COPYING file.
**/

/*
 * Tree of important xkcd.com divs:
 *   #topContainer
 *     #topLeft
 *     #topRight
 *   #middleContainer
 *     #ctitle: the comic's title
 *     #comic: the comic
 *     #transcript
 *   #bottom
 *     #comicLinks
 *     #footnote
 *     #licenseText
 */

require("content-buffer.js");

define_variable('xkcd_add_title', false,
    "When true, xkcd-mode will insert the title caption of the comic "+
    "into the page, below the comic.");

define_variable('xkcd_comic_at_top', false,
    "When true, xkcd-mode will move the header section of the page to"+
    "below the comic section.");

define_variable('xkcd_comic_only', false,
    "When true, xkcd-mode will remove the header and footer sections"+
    "of the page, leaving only the comic section.");

/**
 * xkcd_do_add_title adds the XKCD <img> title text below the image in the
 * page
 */
function xkcd_do_add_title (buffer) {
    var document = buffer.document;
    // Find the <img> tag
    var img = document.evaluate("//div[@id='comic']//img",
        document, null,
        Ci.nsIDOMXPathResult.ANY_TYPE,null).iterateNext();
    if (!img)
        return;
    var title = img.title;
    // In some comics, the <img> is a link, so walk up to the surrounding <A>
    if (img.parentNode.tagName == 'A')
        img = img.parentNode;
    // Insert the text inside a <p> with a known ID
    var text = document.createTextNode(title);
    var span = document.createElement('p');
    span.id = 'conkeror:xkcd-title-text';
    span.appendChild(text);
    img.parentNode.appendChild(span);
}

/**
 * xkcd_rearrange_sections rearranges the sections of the page to put the
 * comic at the top and possibly deleting the other sections (depending on
 * the value of xkcd_comic_only)
 */
function xkcd_rearrange_sections (buffer) {
    var document = buffer.document;
    if (xkcd_comic_only)
        ["topContainer", "bottom"].map(function (elem) { hide_element(buffer, elem) });
    else
        ["topContainer", "bottom"].map(function (elem) { remove_and_readd_element(buffer, elem) });
}

function remove_and_readd_element(buffer, id) {
    var elem = buffer.document.getElementById(id);
    if (! (elem instanceof Ci.nsIDOMNode)) {
        dumpln("xkcd: failed to find dom node for " + id);
    } else {
        let parent = elem.parentNode;
        dumpln("xkcd: move " + id);
        parent.removeChild(elem);
        parent.appendChild(elem);
    }
}

function hide_element(buffer, id) {
    var elem = buffer.document.getElementById(id);
    if (! (elem instanceof Ci.nsIDOMNode)) {
        dumpln("xkcd: failed to find dom node for " + id);
    } else {
        elem.style.display = "none";
    }
}

function unhide_element(buffer, id) {
    var elem = buffer.document.getElementById(id);
    if (! (elem instanceof Ci.nsIDOMNode)) {
        dumpln("xkcd: failed to find dom node for " + id);
    } else {
        elem.style.display = "";
    }
}


function xkcd_perhaps_modify_page (buffer) {
    if (xkcd_add_title)
        xkcd_do_add_title(buffer);

    if (xkcd_comic_only || xkcd_comic_at_top)
        xkcd_rearrange_sections(buffer);
}

define_page_mode("xkcd-mode",
    build_url_regexp($domain = "xkcd",
                     $allow_www = true,
                     $tlds = ["com", "net", "org"],
                     $path = /(\d+\/)?/),
    function enable (buffer) {
        if (buffer.browser.webProgress.isLoadingDocument)
            add_hook.call(buffer, "buffer_dom_content_loaded_hook", xkcd_perhaps_modify_page);
        else
            xkcd_perhaps_modify_page(buffer);
        buffer.page.local.browser_relationship_patterns = {};
        buffer.page.local.browser_relationship_patterns[RELATIONSHIP_NEXT] =
            [new RegExp("\\bnext","i")];
        buffer.page.local.browser_relationship_patterns[RELATIONSHIP_PREVIOUS] =
            [new RegExp("\\bprev","i")];
    },
    function disable (buffer) {
        remove_hook.call(buffer, "buffer_dom_content_loaded_hook", xkcd_perhaps_modify_page);
        // When we disable the mode, remove the <span> and undo div rearranging
        var span = buffer.document.getElementById('conkeror:xkcd-title-text');
        if (span)
            span.parentNode.removeChild(span);

        if (xkcd_comic_only)
            ["topContainer", "bottom"].map(function (elem) { unhide_element(buffer, elem) });
        else
            ["middleContainer", "bottom"].map(function (elem) { remove_and_readd_element(buffer, elem) });
    },
    $display_name = "XKCD");

page_mode_activate(xkcd_mode);

provide("xkcd");
