/**
 * (C) Copyright 2011 Peter Lunicks
 *
 * Use, modification, and distribution are subject to the terms specified in the
 * COPYING file.
**/

define_variable("mouse_wheel_scroll_lines", null,
                "How many lines to scroll when the mouse wheel is used. " +
                "A null value causes mouse-wheel-mode to scroll by the " +
                "number of lines specified in the DOMMouseScroll event.");

function mouse_wheel_scroll (event) {
    var lines;
    if (mouse_wheel_scroll_lines != null)
        lines = mouse_wheel_scroll_lines * (event.detail < 0 ? -1 : 1)
    else
        lines = event.detail;

    // Scroll the frame under mouse, not necessarily the current/focused frame
    var window = this.ownerDocument.defaultView;
    var frame = event.target.ownerDocument.defaultView;
    do_repeatedly(attempt_builtin_command, lines,
                  [window, frame, "cmd_scrollLineDown"],
                  [window, frame, "cmd_scrollLineUp"]);

    // Prevent the scrollbars from scrolling further (if visible)
    event.preventDefault();
}

function mouse_wheel_add_listener (buffer) {
    buffer.browser.addEventListener("DOMMouseScroll",
                                    mouse_wheel_scroll,
                                    true);
}

function mouse_wheel_remove_listener (buffer) {
    buffer.browser.removeEventListener("DOMMouseScroll",
                                       mouse_wheel_scroll,
                                       true);
}

function mouse_wheel_mode_enable () {
    add_hook("create_buffer_hook", mouse_wheel_add_listener);
    for_each_buffer(mouse_wheel_add_listener);
}

function mouse_wheel_mode_disable () {
    remove_hook("create_buffer_hook", mouse_wheel_remove_listener);
    for_each_buffer(mouse_wheel_remove_listener);
}

define_global_mode("mouse_wheel_mode",
                   mouse_wheel_mode_enable,
                   mouse_wheel_mode_disable);

mouse_wheel_mode(true);

provide("mouse-wheel");
